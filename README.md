Scripts for automation and maintenance

1. docker-and-compose-debian10.sh

This script install latest docker and docker-compose on debian 10 and probably on another version deb-like systems(not tested yet).

Check here what version docker-compose you need (for example, the latest release): https://github.com/docker/compose/releases and put it as script`s parameter like ./docker-and-compose-debian10.sh "1.27.4".
